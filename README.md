# SQL Practical Test Instructions

### NOTE - IF YOU ARE USING THE PROVIDED CLOUD DATABASE, PLEASE SKIP ALL SECTIONS BEFORE 'Instructions'

### Prerequisites 
* Install MySQL **8.0.18** https://downloads.mysql.com/archives/get/p/25/file/mysql-installer-web-community-8.0.18.0.msi (Due to a peculiarity in the setup code, versions later than this cause the code to fail with a foreign key error - will fix later)
* Install Git https://git-scm.com/download/win

### Checking Out
From the command line, assuming Git is installed and on your path, change to your desired directory and type:

git clone https://bitbucket.org/costcutter/sql-test

### Contents
The project will be downloaded into a folder called 'sql-test'.  Within this folder there are two files:

* README.md - this document!
* practicaltest.sql - the database setup script

### Database Setup
1. Add the MySQL binaries directory to your path (on Windows: Control Panel -> System -> Advanced System Settings -> Environment Variables -> System Variables).  If installing to the default directory, this will be "C:\Program Files\MySQL\MySQL Server 8.0\bin".
2. Open a command prompt and change directory to the location of practicaltest.sql
3. Assuming step 1 has been followed and MySQL has been added to your path, from the command line, type mysql -u<username> -p<password> to log in to MySQL
4. Then type \\. practicaltest.sql (if not viewing online please ignore first backslash as it is acting as an escape character for the online markup)

Note that some of the data is created using random functions, therefore repeated executions of the setup script will generate different results.

### Instructions
Connect to and study the database.  If you don't have a MySQL database browser installed, you can download one from https://dev.mysql.com/downloads/workbench/ 

If using the provided cloud database, please connect with the following credentials:

* Host: practical-test.c2qqcx7fu6oj.us-east-2.rds.amazonaws.com
* Port: 3306
* Username: test_user
* Password: testDB@123

Answer the following questions:

1.	Draw an entity-relationship diagram for the created database, indicating cardinality and ordinality of relationships where appropriate.  Only database tables are required (individual fields are not).  It is not necessary to use a modelling tool to do this; a scan/photograph etc. will suffice.
2.	Write a query to find the number of vehicles ordered in the last six months for each model.
3.	Write a query to find the first initial, surname and telephone number of all customers who have not yet been invoiced for two or more orders.  The list should be sorted alphabetically (surname takes precedence over forename).
4.	Write a query to display all possible valid combinations of model, engine and equipment as of today's date.  The following fields should be included: model name, engine capacity, fuel type, wheel type and infotainment type.
5.	Write an efficient query to identify all employees whose average sale price **per vehicle** is lower than the overall average for their branch. 
6.  Write an efficient query to display total number of invoices settled in each week of the last 4 completed weeks (a week starts on a Monday).  Each week should be displayed in a separate column. 
7.  Write a query to display a day-by-day running (cumulative) sales total for the entire year to date.
8.  Write a query to replicate the trim_equipment table, replacing effective_date with a start date and and an end date.  Any NULL records should be corrected appropriately.
9.  Write a query to display a string for every employee that shows the reporting line from them to the chief executive (something like 'Employee Name - Line Manager Name - ... - CEO Name').  The query must continue to function correctly should extra levels be added into the hierarchy in the future.
10. Produce a data warehouse design based on the supplied transactional database.  Please provide details of the tables, columns and relationships; an entity-relationship diagram would be an ideal choice for presenting this information.  Please also provide a short supplementary document detailing the reasoning behind your decisions.  If you feel there are some ambiguities, please attempt to resolve these temporarily in a sensible way and include any question(s) you would ask a stakeholder in order to resolve them permanently.

